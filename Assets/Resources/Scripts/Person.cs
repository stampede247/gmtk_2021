using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Person : MonoBehaviour
{
    public Dialogue dialogueBubble;
    public string personName = "New Person";
    public string personNameColor = "blue";
    public string soulmate = "No One";
    public string[] idleTexts;
    public string[] piercedTexts;
    public string[] pairedTexts;
    public float idleMessageDistance = 4;
    public Arrow piercingArrow = null;
    public bool isPaired = false;
    public Person pairedPerson = null;
    public SpringJoint pairJoint = null;

    private Rigidbody thisBody;
    private Player playerObj;
    private int idleTextIndex = 0;
    private int piercedTextIndex = 0;
    private int pairedTextIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        idleTextIndex = Random.Range(0, idleTexts.Length);
        piercedTextIndex = Random.Range(0, piercedTexts.Length);
        pairedTextIndex = Random.Range(0, pairedTexts.Length);
        thisBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.playerObj == null)
        {
            this.playerObj = GameObject.FindObjectOfType<Player>();
        }
        
        if (this.isPaired && !dialogueBubble.isShowing)
        {
            if (pairedTexts.Length > 0)
            {
                this.dialogueBubble.Show(this.personName, this.personNameColor, this.pairedTexts[pairedTextIndex % pairedTexts.Length], true);
                this.pairedTextIndex++;
            }
            else
            {
                this.dialogueBubble.Show(this.personName, this.personNameColor, "[No Pair Text]", true);
            }
        }

        if (this.piercingArrow != null && !dialogueBubble.isShowing)
        {
            this.dialogueBubble.Show(this.personName, this.personNameColor, this.piercedTexts[piercedTextIndex % piercedTexts.Length], false);
            this.piercedTextIndex++;
        }

        bool playerIsClose = false;
        if (this.playerObj != null)
        {
            Vector3 playerOffset = this.playerObj.transform.position - this.transform.position;
            Vector2 playerOffset2D = new Vector2(playerOffset.x, playerOffset.z);
            if (playerOffset2D.magnitude < this.idleMessageDistance)
            {
                playerIsClose = true;
            }
        }
        if (playerIsClose && !dialogueBubble.isShowing)
        {
            this.dialogueBubble.Show(this.personName, this.personNameColor, this.idleTexts[idleTextIndex % this.idleTexts.Length], false);
            this.idleTextIndex++;
        }

        if (this.piercingArrow == null && !playerIsClose && !this.isPaired)
        {
            if (dialogueBubble.isShowing)
            {
                this.dialogueBubble.Hide();
            }
        }
    }

    public void MakePair(Person otherPerson, GameObject pairPrefab)
    {
        Debug.Log("Pairing \"" + this.name + "\" and \"" + otherPerson.name + "\"");

        this.isPaired = true;
        this.pairedPerson = otherPerson;
        otherPerson.pairedPerson = this;
        otherPerson.isPaired = true;
        if (this.dialogueBubble.isShowing) { this.dialogueBubble.Hide(); }
        if (otherPerson.dialogueBubble.isShowing) { otherPerson.dialogueBubble.Hide(); }

        //pairJoint = thisBody.gameObject.AddComponent<SpringJoint>();
        //pairJoint.connectedBody = otherPerson.GetComponent<Rigidbody>();
        //pairJoint.minDistance = 0;
        //pairJoint.maxDistance = 0f;
        //pairJoint.spring = 10;
        //pairJoint.damper = 0.2f;
        //pairJoint.anchor = new Vector3(0, 0, 0);
        //pairJoint.connectedAnchor = new Vector3(0, 0, 0);

        GameObject pairObj = GameObject.Instantiate(pairPrefab, this.transform.position, Quaternion.identity);
        PairHandler pairHandler = pairObj.GetComponent<PairHandler>();
        Assert.IsNotNull(pairHandler);
        pairHandler.person1 = this;
        pairHandler.person2 = otherPerson;
    }
}
