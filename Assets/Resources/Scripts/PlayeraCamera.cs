using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class PlayeraCamera : MonoBehaviour
{
    public Player playerObj;

    private Vector3 cameraOffset;

    // Start is called before the first frame update
    void Start()
    {
        Assert.IsNotNull(playerObj);
        this.cameraOffset = this.transform.position - playerObj.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = playerObj.transform.position + this.cameraOffset;
    }
}
