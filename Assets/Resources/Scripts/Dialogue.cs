using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    public TextMeshPro textComp;
    public SpriteRenderer bubbleBack;

    private Sprite[] backSprites;
    private string nameStr;
    private string nameColor;
    private string dialogueStr;
    private bool isPairedDialogue;
    public bool isShowing = false;
    private float openAnimTime = 0.0f;
    private int backFrameNum = 0;
    private float textAnimTime = 0.0f;
    private int numCharsDisplayed = 0;

    private string richText;
    private List<RangeInt> richTextMetaRanges;
    private int richTextNumChars;

    // Start is called before the first frame update
    void Start()
    {
        Assert.IsNotNull(this.textComp);
        Assert.IsNotNull(this.bubbleBack);
        this.backSprites = Resources.LoadAll<Sprite>("Sprites/bubble_back");
        this.bubbleBack.enabled = false;
        this.textComp.enabled = false;
        this.isShowing = false;
    }

    int AnalyzeRichText(string text, out List<RangeInt> metaRanges)
    {
        //Debug.Log("Parsing text \"" + text + "\"");
        int result = text.Length;
        metaRanges = new List<RangeInt>();
        int inMetaTag = 0;
        int metaTagStart = 0;
        for (int cIndex = 0; cIndex < text.Length; cIndex++)
        {
            if (text[cIndex] == '<')
            {
                if (inMetaTag == 0) { metaTagStart = cIndex; }
                inMetaTag++;
            }
            else if (text[cIndex] == '>')
            {
                if (inMetaTag > 0)
                {
                    inMetaTag--;
                    if (inMetaTag == 0)
                    {
                        int rangeLength = cIndex + 1 - metaTagStart;
                        metaRanges.Add(new RangeInt(metaTagStart, rangeLength));
                        //Debug.Log("Found range [" + metaTagStart + ", " + (cIndex + 1) + "]");
                        result -= rangeLength;
                    }
                }
            }
        }
        //Debug.Log("Found " + metaRanges.Count + " ranges (" + result + " chars)");
        return result;
    }

    public void Show(string name, string nameColor, string dialogue, bool isPairedDialogue)
    {
        this.nameStr = name;
        this.nameColor = nameColor;
        this.dialogueStr = dialogue;
        this.isPairedDialogue = isPairedDialogue;
        this.openAnimTime = 0.0f;
        this.backFrameNum = 0;
        this.bubbleBack.sprite = backSprites[this.backFrameNum];
        this.textAnimTime = 0.0f;
        this.numCharsDisplayed = 0;
        this.isShowing = true;
        this.bubbleBack.enabled = true;
        this.textComp.enabled = false;
        this.textComp.text = "";

        this.richText = "<indent=2><color=" + this.nameColor + ">" + this.nameStr + " - <color=black></indent>" + this.dialogueStr;
        this.richTextNumChars = AnalyzeRichText(this.richText, out this.richTextMetaRanges);
        this.bubbleBack.color = (this.isPairedDialogue ? new Color(255, 200, 200) : Color.white);
    }
    public void Hide()
    {
        if (this.isShowing)
        {
            this.isShowing = false;
        }
    }

    float TimeScaledAnim(float animPeriod, float tDelta)
    {
        return ((1000.0f / 60.0f) * tDelta) / animPeriod;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float tDelta = Time.fixedDeltaTime / (1 / 60.0f);

        if (this.isShowing)
        {
            if (this.openAnimTime < 1.0f)
            {
                this.openAnimTime += TimeScaledAnim(300, tDelta);
                if (this.openAnimTime >= 1.0f) { this.openAnimTime = 1.0f; }
            }
            else if (this.textAnimTime < 1.0f)
            {
                this.textAnimTime += TimeScaledAnim(30 * this.richTextNumChars, tDelta);
                if (this.textAnimTime >= 1.0f) { this.textAnimTime = 1.0f; }
            }
        }
        else if (this.bubbleBack.enabled)
        {
            if (this.textAnimTime > 0.0f)
            {
                this.textAnimTime -= TimeScaledAnim(300, tDelta);
                if (this.textAnimTime <= 0.0f) { this.textAnimTime = 0.0f; }
            }
            else if (this.openAnimTime > 0.0f)
            {
                this.openAnimTime -= TimeScaledAnim(300, tDelta);
                if (this.openAnimTime <= 0.0f) { this.openAnimTime = 0.0f; }
            }
        }
    }

    private void Update()
    {
        if (this.bubbleBack.enabled)
        {
            int newFrameNum = Mathf.FloorToInt(this.openAnimTime * 4);
            if (newFrameNum > this.backSprites.Length - 1) { newFrameNum = this.backSprites.Length - 1; }
            if (this.backFrameNum != newFrameNum)
            {
                this.bubbleBack.sprite = backSprites[newFrameNum];
                this.backFrameNum = newFrameNum;
            }

            int newNumChars = Mathf.FloorToInt(this.textAnimTime * this.richTextNumChars);
            if (newNumChars > this.richText.Length) { newNumChars = this.richTextNumChars; }
            if (this.numCharsDisplayed != newNumChars)
            {
                RangeInt displayRange = new RangeInt(0, newNumChars);
                foreach (RangeInt metaRange in this.richTextMetaRanges)
                {
                    if (metaRange.start < displayRange.end)
                    {
                        displayRange.length += metaRange.length;
                    }
                }
                this.textComp.text = this.richText.Substring(displayRange.start, displayRange.length);
                this.numCharsDisplayed = newNumChars;
            }
        }
        if (this.textAnimTime <= 0.0f && this.textComp.enabled) { this.textComp.enabled = false; }
        if (this.textAnimTime > 0.0f && !this.textComp.enabled) { this.textComp.enabled = true; }
        if (this.openAnimTime <= 0.0f && this.bubbleBack.enabled) { this.bubbleBack.enabled = false; }
        if (this.openAnimTime > 0.0f && !this.bubbleBack.enabled) { this.bubbleBack.enabled = true; }

        this.transform.rotation = Quaternion.identity;
    } 
}
