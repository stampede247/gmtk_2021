using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicHandler : MonoBehaviour
{
    public FMODUnity.StudioEventEmitter musicEventEmitter;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetProgress(float progress)
    {
        if (musicEventEmitter != null)
        {
            musicEventEmitter.SetParameter("progress", progress);
        }
    }
}
