using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;


public class OrientObjectToCameraPlane : MonoBehaviour
{
    public Transform transformToRotate;
    public Transform transformWithTargetRotation;
    public Quaternion targetRotation;
    public float maxRotationAnglePerIteration;

    // Start is called before the first frame update
    void Start() {
        if (this.transformWithTargetRotation != null)
        {
            this.targetRotation = this.transformWithTargetRotation.rotation;
        }
    } 

    // Update is called once per frame
    void Update()
    {
    }

    private void FixedUpdate()
    {
        this.transformToRotate.rotation = Quaternion.RotateTowards(this.transformToRotate.rotation, this.targetRotation, maxRotationAnglePerIteration); //* Time.fixedDeltaTime);
    }
}
