using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Arrow : MonoBehaviour
{
    public GameObject arrowObj;
    public GameObject attachmentObj;

    public bool hitSurface = false;
    public Person connectedPerson = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CollisionOccurred(ArrowBody arrBody, Collision collision)
    {
        if (hitSurface) { return; }
        Rigidbody thisBody = arrowObj.GetComponent<Rigidbody>();
        Assert.IsNotNull(thisBody);
        Rigidbody hitBody = collision.gameObject.GetComponentInChildren<Rigidbody>();
        bool playHeartsSound = false;
        if (hitBody != null && !hitBody.isKinematic)
        {
            Debug.Log("Arrow hit a dynamic object");
            hitSurface = true;
            FixedJoint newJoint = thisBody.gameObject.AddComponent<FixedJoint>();
            newJoint.connectedBody = hitBody;
            arrowObj.layer = LayerMask.NameToLayer("NoCollision");
        }
        else
        {
            Debug.Log("Arrow hit solid surface");
            arrowObj.GetComponent<Rigidbody>().isKinematic = true;
            hitSurface = true;
        }

        Person piercedPerson = collision.gameObject.GetComponent<Person>();
        if (piercedPerson != null)
        {
            Debug.Log("Arrow hit a person!");
            if (this.connectedPerson != null) { this.connectedPerson.piercingArrow = null; }
            this.connectedPerson = piercedPerson;
            piercedPerson.piercingArrow = this;
            if (piercedPerson.dialogueBubble.isShowing && !piercedPerson.isPaired) { piercedPerson.dialogueBubble.Hide(); }
            playHeartsSound = true;
            ParticleSystem heartPartSys = this.GetComponentInChildren<ParticleSystem>(true);
            if (heartPartSys != null)
            {
                heartPartSys.transform.position = arrowObj.transform.position;
                heartPartSys.Emit(10);
            }
        }

        if (playHeartsSound) { FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/NPC_ArrowHit", this.transform.position); }
        else { FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/Arrow_Hit", this.transform.position); }
    }
}
