using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGamePrompt : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject ObjectToToggleActive;
    void Start()
    {
        
    }

    public void OpenPrompt()
    {
        ObjectToToggleActive.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DontQuit()
    {
        //hide window
        gameObject.SetActive(false);
    }

    public void ConfirmQuit()
    {
        Application.Quit();
    }
}
