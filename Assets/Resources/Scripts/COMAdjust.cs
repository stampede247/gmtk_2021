using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class COMAdjust : MonoBehaviour
{

    public Vector3 comOffset;
    public Rigidbody selfRigidBody;

    // Start is called before the first frame update
    void Start()
    {
        Assert.IsNotNull(this.selfRigidBody);
        selfRigidBody.centerOfMass = (selfRigidBody.centerOfMass + comOffset);
    }

    // Update is called once per frame
    void Update() {}
}
