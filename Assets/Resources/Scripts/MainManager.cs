using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainManager : MonoBehaviour
{
    public Player playerRef;
    public MusicHandler musicHandler;
    public MainUi mainUi;
    public PersonPair[] pairs;
    public float progress = 0.0f;
    
    public class PersonPair
    {
        bool completed;
        string person1;
        string person2;
        Sprite icon1;
        Sprite icon2;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (musicHandler != null)
        {
            musicHandler.SetProgress(this.progress);
        }
    }
}
