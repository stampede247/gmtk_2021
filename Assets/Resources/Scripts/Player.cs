using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Assertions;
using UnityEngine.UI;

// TODO this code should be put into, like, 4 separate classes

public class Player : MonoBehaviour
{
    public float groundAccel = 0.004f;
    public float groundDecel = 0.1f; //percent
    public float airAccel = 0.001f;
    public float airDecel = 0.02f;
    public float gravity = 0.0025f;
    public float maxSpeed = 0.08f;
    public float maxRunSpeed = 0.12f;
    public float terminalVelocity = 0.4f;
    public float jumpSpeed = 0.08f;
    public float arrowSpeed = 8;
    public float arrowLobAmount = 0.1f;
    public SpriteRenderer bodySprite;
    public SpriteRenderer legsSprite;
    public GameObject arrowPrefab;
    public Transform arrowSpawnPos;
    public Transform arrowSpawnPlane;
    public GameObject bowObj;
    public bool hasBow = false;
    public float progress = 0;
    public GameObject pairPrefab;

    public enum AnimState
    {
        Idle = 0,
        Walking,
    }

    private PlayerInput playerInput;
    //private Rigidbody body;
    private CharacterController charController;
    private Vector3 velocity = Vector3.zero;
    private Text debugText = null;
    private bool groundedLastFrame = false;
    private Sprite[] bodySpriteSheet;
    private Sprite[] legsSpriteSheet;
    private AnimState animState = AnimState.Idle;
    private float animTime;
    private int currentBodyFrame = 0;
    private int currentLegsFrame = 0;
    private bool facingRight = true;
    private Arrow arrowRef = null;
    int[] legWalkFrameWeights = { 1, 2, 1, 2 };

    private GameObject bowSpriteObj;

    // Start is called before the first frame update
    void Start()
    {
        this.playerInput = GetComponent<PlayerInput>();
        Assert.IsNotNull(this.playerInput);
        //this.body = GetComponent<Rigidbody>();
        //Assert.IsNotNull(this.body);
        this.charController = GetComponent<CharacterController>();
        Assert.IsNotNull(this.charController);
        debugText = GameObject.Find("DebugText").GetComponent<Text>();
        if (debugText == null)
        {
            Debug.Log("Couldn't find DebugText game object");
        }
        bodySpriteSheet = Resources.LoadAll<Sprite>("Sprites/cupid_body");
        Debug.Log("Found " + bodySpriteSheet.Length + " cupid body sprites");
        Assert.IsNotNull(bodySprite);
        legsSpriteSheet = Resources.LoadAll<Sprite>("Sprites/cupid_legs");
        Debug.Log("Found " + legsSpriteSheet.Length + " cupid legs sprites");
        Assert.IsNotNull(legsSprite);

        bowSpriteObj = this.bowObj.GetComponentInChildren<SpriteRenderer>().gameObject;
    }

    float TimeScaledAnim(float animPeriod, float tDelta)
    {
        return ((1000.0f / 60.0f) * tDelta) / animPeriod;
    }
    int ChooseAnimFrame(float period, int startFrame, int numFrames)
    {
        return startFrame + (int)(numFrames * (Mathf.Repeat(Time.fixedTime * 1000.0f, period) / period));
    }
    int ChooseAnimFrameByWeights(float period, int startFrame, int[] frameWeights)
    {
        int weightsTotal = 0;
        foreach (int weight in frameWeights) { weightsTotal += weight; }
        float periodPercent = (Mathf.Repeat(Time.fixedTime * 1000.0f, period) / period);
        int unweightedIndex = Mathf.FloorToInt(weightsTotal * periodPercent);
        int result = 0;
        while (result < frameWeights.Length && unweightedIndex >= frameWeights[result]) { unweightedIndex -= frameWeights[result]; result++; }
        return result;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (debugText != null) { debugText.text = ""; }

        float tDelta = Time.fixedDeltaTime / (1 / 60.0f);
        if (debugText != null) { debugText.text += $"t { tDelta: 0.####}\n"; }

        Vector2 inputVec = new Vector2(playerInput.actions["MoveX"].ReadValue<float>(), playerInput.actions["MoveY"].ReadValue<float>());
        bool isGrounded = charController.isGrounded || groundedLastFrame;
        //isGrounded = true; //TODO: Remove me!
        if (debugText != null) { debugText.text += $"grounded " + (isGrounded ? "True" : "False") + "\n"; }

        velocity.y -= gravity;
        if (velocity.y < -terminalVelocity) { velocity.y = -terminalVelocity; }
        float moveSpeed = new Vector2(velocity.x, velocity.z).magnitude;
        float moveDir = Mathf.Atan2(velocity.z, velocity.x);

        if (isGrounded && playerInput.actions["Jump"].triggered)
        {
            velocity.y = jumpSpeed;
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/Player_Jump", this.transform.position);
        }

        Vector3 targetPos = Vector3.zero;
        Vector3 targetDirVec = Vector3.zero;
        // Read mouse position, find if it intersect with the plane through point arrowSpawnPlane (?)
        Vector2 mousePosition = playerInput.actions["MousePosition"].ReadValue<Vector2>();
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(mousePosition.x, mousePosition.y, 0));
        // TODO it seems unnecessary to generate this every fixed update?
        Plane targetingPlane = new Plane(Vector3.up, arrowSpawnPlane.position);
        bool targetPosDecided = false;
        float hitDistance = 0;
        // If it intersects, store the direction the arrow should go in targetDirVec
        if (targetingPlane.Raycast(ray, out hitDistance))
        {
            targetPosDecided = true;
            targetPos = ray.origin + ray.direction * hitDistance;
            targetDirVec = targetPos - arrowSpawnPlane.transform.position;
        }

        if (this.hasBow && this.bowObj != null)
        {
            if (!this.bowObj.activeSelf) { this.bowObj.SetActive(true); }
            
            /*if (this.facingRight)
            {
                bowSpriteObj.transform.position = new Vector3(this.bowObj.transform.position.x - 0.33f, bowSpriteObj.transform.position.y, bowSpriteObj.transform.position.z);
                bowSpriteObj.GetComponent<SpriteRenderer>().flipX = false;
            }
            else
            {
                bowSpriteObj.transform.position = new Vector3(this.bowObj.transform.position.x + 0.33f, bowSpriteObj.transform.position.y, bowSpriteObj.transform.position.z);
                bowSpriteObj.GetComponent<SpriteRenderer>().flipX = true;
            }*/
            
            // If there's an arrow already fired, point towards the arrow 
            if (arrowRef != null) {
                Vector3 arrowDirVec = this.transform.position - this.arrowRef.arrowObj.transform.position;
                float firingAngle = -Mathf.Atan2(arrowDirVec.z, arrowDirVec.x);
                bowObj.transform.eulerAngles = new Vector3(0, firingAngle*Mathf.Rad2Deg+180f, 0);
            } 
            // Else, point in target direction
            else if (targetPosDecided)
            {
                float firingAngle = -Mathf.Atan2(targetDirVec.z, targetDirVec.x);
                bowObj.transform.eulerAngles = new Vector3(0, firingAngle*Mathf.Rad2Deg, 0);
            }
            
            
        }

        if (playerInput.actions["Fire"].triggered && this.hasBow)
        {
            // If arrow has already been shot, delete arrow
            if (arrowRef != null)
            {
                GameObject.Destroy(this.arrowRef.gameObject);
                arrowRef = null;
                FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/Arrow_Load", this.transform.position);
            }
            // Else, create
            else
            {
                if (targetPosDecided)
                {
                    Vector3 velocityDirVec = targetPos - arrowSpawnPlane.transform.position;
                    velocityDirVec.Normalize();
                    velocityDirVec.y += arrowLobAmount;
                    velocityDirVec.Normalize();
                    GameObject newArrow = Instantiate(arrowPrefab, arrowSpawnPos.position, Quaternion.identity);
                    Assert.IsNotNull(newArrow);
                    Arrow newArrowRef = newArrow.GetComponent<Arrow>();
                    Assert.IsNotNull(newArrowRef);
                    Assert.IsNotNull(newArrowRef.arrowObj);
                    Assert.IsNotNull(newArrowRef.attachmentObj);
                    newArrowRef.arrowObj.transform.rotation = Quaternion.FromToRotation(Vector3.right, velocityDirVec);
                    newArrowRef.arrowObj.GetComponent<Rigidbody>().velocity = velocityDirVec * arrowSpeed;
                    if (this.arrowRef != null)
                    {
                        Debug.Log("Destroying old arrow");
                        GameObject.Destroy(this.arrowRef.gameObject);
                    }
                    this.arrowRef = newArrowRef;
                    FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/Arrow_Fire", this.transform.position);
                }

                //RaycastHit hit;
                //if (Physics.Raycast(ray, out hit, 50f))
                //{
                //    //Debug.DrawLine(ray.origin, hit.point);
                //    newArrow = Instantiate(arrowPrefab, hit.point, Quaternion.identity);
                //}
            }
        }

        float actualMaxSpeed = (playerInput.actions["Run"].phase == InputActionPhase.Started) ? this.maxRunSpeed : this.maxSpeed;
        if (inputVec.magnitude > 0.01f)
        {
            inputVec.Normalize();
            if (isGrounded)
            {
                float inputDir = Mathf.Atan2(inputVec.y, inputVec.x);
                moveDir = inputDir;
                if (moveSpeed < maxSpeed)
                {
                    moveSpeed += groundAccel * tDelta;
                    if (moveSpeed > maxSpeed) { moveSpeed = maxSpeed; }
                }
            }
            else
            {
                Vector2 horVelocity = new Vector2(velocity.x, velocity.z);
                if (horVelocity.magnitude < maxSpeed)
                {
                    horVelocity += inputVec * airAccel;
                    if (horVelocity.magnitude > maxSpeed) { horVelocity = horVelocity.normalized * maxSpeed; }
                }
                velocity.x = horVelocity.x;
                velocity.z = horVelocity.y;
                moveSpeed = new Vector2(velocity.x, velocity.z).magnitude;
                moveDir = Mathf.Atan2(velocity.z, velocity.x);
            }
        }
        else
        {
            if (isGrounded)
            {
                moveSpeed *= 1.0f - groundDecel * tDelta;
            }
            else
            {
                moveSpeed *= 1.0f - airDecel * tDelta;
            }
        }

        velocity = new Vector3(Mathf.Cos(moveDir) * moveSpeed, velocity.y, Mathf.Sin(moveDir) * moveSpeed);
        CollisionFlags colFlags = this.charController.Move(new Vector3(velocity.x, 0, velocity.z) * tDelta);
        colFlags |= this.charController.Move(new Vector3(0, velocity.y, 0) * tDelta);
        if ((colFlags & CollisionFlags.Below) != 0 && velocity.y < 0) { velocity.y = 0; }

        if (debugText != null) { debugText.text += $"speed {moveSpeed:0.####} dir {moveDir:0.####}\n"; }
        if (debugText != null) { debugText.text += $"Velocity: ({this.velocity.x:0.####}, {this.velocity.y:0.####}, {this.velocity.z:0.####})\n"; }
        if ((colFlags & CollisionFlags.Below) != 0) { groundedLastFrame = true; }
        else { groundedLastFrame = false; }

        if (moveSpeed > 0.01f && animState == AnimState.Idle)
        {
            animState = AnimState.Walking;
            animTime = 0.0f;
        }
        if (moveSpeed <= 0.01f && animState == AnimState.Walking)
        {
            animState = AnimState.Idle;
            animTime = 0.0f;
        }

        if (velocity.x < -0.01)
        {
            facingRight = false;
        }
        else if (velocity.x > 0.01)
        {
            facingRight = true;
        }

        bool flipSprites = !facingRight;
        int bodyAnimFrame = this.currentBodyFrame;
        int legsAnimFrame = this.currentLegsFrame;
        if (animState == AnimState.Walking)
        {
            //animTime += TimeScaledAnim(500, tDelta);
            //if (animTime >= 1.0f) { animTime = Mathf.Repeat(animTime, 1.0f);  }
            //newAnimFrame = Mathf.FloorToInt(animTime * 4);
            legsAnimFrame = ChooseAnimFrameByWeights(600, 0, legWalkFrameWeights);
        }
        else
        {
            legsAnimFrame = 0;
        }

        if (this.bodySprite.flipX != flipSprites) { this.bodySprite.flipX = flipSprites; }
        if (this.legsSprite.flipX != flipSprites) { this.legsSprite.flipX = flipSprites; }
        if (this.currentBodyFrame != bodyAnimFrame)
        {
            this.bodySprite.sprite = bodySpriteSheet[bodyAnimFrame];
            this.currentBodyFrame = bodyAnimFrame;
        }
        if (this.currentLegsFrame != legsAnimFrame)
        {
            this.legsSprite.sprite = legsSpriteSheet[legsAnimFrame];
            this.currentLegsFrame = legsAnimFrame;
        }

        if (this.arrowRef != null)
        {
            this.arrowRef.attachmentObj.transform.position = this.arrowSpawnPos.position;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Debug.Log("Found Collision volume");
        if (this.arrowRef != null)
        {
        	Person draggingPerson = this.arrowRef.connectedPerson;
            Person otherPerson = other.GetComponentInParent<Person>();
            if (draggingPerson != null && !draggingPerson.isPaired && otherPerson != null && !otherPerson.isPaired && draggingPerson.name != otherPerson.name)
            {
                Debug.Log($"Found Persons {draggingPerson.name} and {otherPerson.name}!");
                // if (otherPerson.name == draggingPerson.soulmate)
                {
                    GameObject.Destroy(this.arrowRef.gameObject);
                    this.arrowRef = null;
                    FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/Heart_Sound", this.transform.position);
                    otherPerson.MakePair(draggingPerson, pairPrefab);
                }
            }
        }
    }
}
