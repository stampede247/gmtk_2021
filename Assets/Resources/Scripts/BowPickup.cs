using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowPickup : MonoBehaviour
{
    private bool isAlive = true;
    private Vector3 basePos;

    // Start is called before the first frame update
    void Start()
    {
        basePos = this.transform.position;
    }

    float Oscillate(float period, float min = 0, float max = 1)
    {
        return min + (max - min) * (Mathf.Sin(((Time.fixedTime * 1000.0f) * 2 * Mathf.PI) / period) + 1) / 2;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Fixed time: " + Time.fixedTime);
        this.transform.position = basePos + new Vector3(0, Oscillate(1000, -0.2f, 0.2f), 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!this.isAlive) { return; }
        Player player = other.GetComponent<Player>();
        if (player != null)
        {
            Debug.Log("Player got the bow!");
            player.hasBow = true;
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/Bow_Pickup");
            //TODO: Spawn some particles!
            Destroy(gameObject);
            this.isAlive = false;
        }
    }
}
