using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiPairEntry : MonoBehaviour
{
    public Image leftImage;
    public Image rightImage;
}
