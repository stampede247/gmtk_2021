using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ArrowBody : MonoBehaviour
{
    public Arrow arrowObj;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        Assert.IsNotNull(arrowObj);
        arrowObj.CollisionOccurred(this, collision);
    }
}
