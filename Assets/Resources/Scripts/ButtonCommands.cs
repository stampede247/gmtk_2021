using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonCommands : MonoBehaviour
{
    // Start is called before the first frame update
    public int sceneIntToLoad;
    public FadeImageInOut canvasFader;

    GameObject Menu;
    bool Pause = false;

    void Start()
    {
        
    }

    public void StartGame()
    {
        StartCoroutine("FirstLevelStarts");
    }

    public void CloseGame()
    {
        Application.Quit();
    }

    IEnumerator FirstLevelStarts()
    {
        //canvasFader.FadeIn();
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(sceneIntToLoad);
        yield return null;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
