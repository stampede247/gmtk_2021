using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class PairHandler : MonoBehaviour
{
    public ParticleSystem partSystem;
    public Person person1 = null;
    public Person person2 = null;

    // Start is called before the first frame update
    void Start()
    {
        Assert.IsNotNull(partSystem);
    }

    // Update is called once per frame
    void Update()
    {
        if (person1 != null && person2 != null)
        {
            partSystem.transform.position = (person1.transform.position + person2.transform.position) / 2;
            partSystem.transform.rotation = Quaternion.LookRotation(person2.transform.position - person1.transform.position, Vector3.up);
            float edgeRadius = (person1.transform.position - person2.transform.position).magnitude / 2;
            //partSystem.transform.localScale = new Vector3(edgeRadius, edgeRadius, edgeRadius);
            ParticleSystem.ShapeModule partShape = partSystem.shape;
            partShape.radius = edgeRadius;
        }
    }
}
